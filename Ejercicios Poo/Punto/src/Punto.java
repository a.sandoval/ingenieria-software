public class Punto {
    private int x,y;

    Punto() { this.x=this.y=1;}
    Punto(int a, int b) {this.x = a; this.y = b;}
    Punto(Punto p) {this.x = p.getX(); this.y = p.getY();}

    public void set(int a, int b) {
        this.x = a;
        this.y = b;
    }
    public void set(double r){
        this.x = this.y = (int) r;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Punto yo() {return this;}

    public Punto mueva(int deltaX, int deltaY){
        Punto p = new Punto(this);
        p.set(p.getX()+deltaX, p.getY()+deltaY);
        return p;
    }

    public void muestra(){
        System.out.print("x: ");
        System.out.println(this.x);
        System.out.print("y: ");
        System.out.println(this.y);
    }
}
